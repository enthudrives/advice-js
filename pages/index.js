import Head from 'next/head';
import { useState } from "react";
import styles from '../styles/Home.module.css';

export default function Home() {
  const [randomAdvice, setRandomAdvice] = useState("")
  const getRandomAdvice = async () => {
    const randomAdvice = await fetch("/api/advice")
    const payload = await randomAdvice.json()
    const advice = payload?.data?.slip?.advice;
    setRandomAdvice(advice)
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>Kyns bank</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

        <main>
            <h2>    
                {randomAdvice}
            </h2>
            <button onClick={getRandomAdvice} className="button-3">
                Hit me!
            </button>
        </main>
        <style jsx>{`
            main {
                padding: 5rem 0;
                flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        footer img {
          margin-left: 0.5rem;
        }
        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
          text-decoration: none;
          color: inherit;
        }
        
         .button-3 {
             appearance: none;
             background-color: #2ea44f;
             border: 1px solid rgba(27, 31, 35, .15);
             border-radius: 6px;
             box-shadow: rgba(27, 31, 35, .1) 0 1px 0;
             box-sizing: border-box;
             color: #fff;
             cursor: pointer;
             display: inline-block;
             font-family: -apple-system,system-ui,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji";
             font-size: 14px;
             font-weight: 600;
             line-height: 20px;
             padding: 6px 16px;
             position: relative;
             text-align: center;
             text-decoration: none;
             user-select: none;
             -webkit-user-select: none;
             touch-action: manipulation;
             vertical-align: middle;
             white-space: nowrap;
         }

            .button-3:focus:not(:focus-visible):not(.focus-visible) {
                box-shadow: none;
                outline: none;
            }

            .button-3:hover {
                background-color: #2c974b;
            }

            .button-3:focus {
                box-shadow: rgba(46, 164, 79, .4) 0 0 0 3px;
                outline: none;
            }

            .button-3:disabled {
                background-color: #94d3a2;
                border-color: rgba(27, 31, 35, .1);
                color: rgba(255, 255, 255, .8);
                cursor: default;
            }

            .button-3:active {
                background-color: #298e46;
                box-shadow: rgba(20, 70, 32, .2) 0 1px 0 inset;
            }
        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family:
            Menlo,
            Monaco,
            Lucida Console,
            Liberation Mono,
            DejaVu Sans Mono,
            Bitstream Vera Sans Mono,
            Courier New,
            monospace;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family:
            -apple-system,
            BlinkMacSystemFont,
            Segoe UI,
            Roboto,
            Oxygen,
            Ubuntu,
            Cantarell,
            Fira Sans,
            Droid Sans,
            Helvetica Neue,
            sans-serif;
        }
        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
