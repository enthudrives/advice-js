import { NextResponse} from "next/server";
import { Database } from 'fakebase';

const db = new Database('./data/');
const User = db.table('users');

async function handler(req, res) {
    if (req.method === 'GET') {
        var password='p@ssw0rd'
        var str = password
        var time = str.replace(/\n$/, '');
        const result = await fetch('https://api.adviceslip.com/advice', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        const data = await result.json()

        res.status(200).json({ message: 'Success', data });
    } else {
        res.status(404).json({ message: "no path found" });
    }
}
export default handler;