FROM node:18.13.0
LABEL authors="suganthi"

RUN mkdir /usr/src/advice-js
RUN mkdir /tmp/extracted_files
COPY . /usr/src/advice-js
WORKDIR /usr/src/advice-js

RUN npm update
RUN npm install
EXPOSE 3001
EXPOSE 9229
ENTRYPOINT ["npm", "start"]